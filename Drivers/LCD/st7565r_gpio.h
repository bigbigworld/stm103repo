#ifndef _st7565r_gpio_h_
#define _st7565r_gpio_h_

uint16_t GPIO_ReadOutputData(GPIO_TypeDef* GPIOx);

void GPIO_Write(GPIO_TypeDef* GPIOx, uint16_t PortVal);

void GPIO_SetBits(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

void GPIO_ResetBits(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

void LCD_GPIO_Configuration(void);

void LCD_GPIO_Init(void);

void LCD_GPIO_Configuration_read(void);

uint16_t GPIO_ReadInputData(GPIO_TypeDef* GPIOx);
#endif
