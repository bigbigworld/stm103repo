#ifndef _st7565r_h_
#define _st7565r_h_
#include "st7565r_gpio.h"

#define LCD_REVERSE 1

#if defined(LCD_REVERSE)
	#define Max_Column 132
#else
	#define Max_Column 128
#endif
#define CS_1  GPIO_SetBits(GPIOA, GPIO_PIN_8)    //取消LCD片选
#define CS_0  GPIO_ResetBits(GPIOA, GPIO_PIN_8)  //LCD片选有效

#define RES_1 GPIO_SetBits(GPIOA, GPIO_PIN_4)    //LCD低电平复位，复位之后保持高电平
#define RES_0 GPIO_ResetBits(GPIOA, GPIO_PIN_4)  //LCD复位

#define A0_1  GPIO_SetBits(GPIOA, GPIO_PIN_5)   //Ao=1,向LCD输入数据
#define A0_0  GPIO_ResetBits(GPIOA, GPIO_PIN_5) //Ao=0,向LCD写入指令

#define WR_1  GPIO_SetBits(GPIOA, GPIO_PIN_6)   //LCD写禁止
#define WR_0  GPIO_ResetBits(GPIOA, GPIO_PIN_6) //LCD写允许

#define RD_1  GPIO_SetBits(GPIOA, GPIO_PIN_7)   //LCD读禁止
#define RD_0  GPIO_ResetBits(GPIOA, GPIO_PIN_7) //LCD读允许


void Lcd12232delay(unsigned int Time);//延时函数

void LCD_WriteLByte(uint8_t Byte);//写PC口的低8位，高8位不变

void w_com(unsigned char Byte);//向LCD写指令

void w_data(unsigned char data);//向LCD写数据

void SetStartPage(uint8_t StartPageAddress);//设置起始页面地址

void SetStartColumn(uint8_t StartColumnAddress);//设置起始列地址

void SetStartLine(uint8_t StartLineAddress);//设置起始行地址

void clrscr(void);//清屏

void ClrOnePage(uint8_t StartPage);//清除一页

void ClrNumColumn(unsigned char StartPage,unsigned char StartColumn,unsigned char Num);//清除StartPage页从StartColumn个汉字位置开始的Num列

//从StartPage页的第Column个字符位置开始显示Num个8*16的字符
void DisplayZf(unsigned char StartPage,unsigned char column,unsigned char Num,const unsigned char *p);
//从StartPage页的第Column个字符位置显示16*16的汉字
void disp_ch(uint8_t c,uint8_t StartPage,uint8_t column);
void reverse_disp_ch(uint8_t c,uint8_t StartPage,uint8_t column);
void display_graphic_12x16(unsigned char *p,unsigned char StartPage,unsigned char column);
void reverse_display_graphic_12x16(unsigned char *p,unsigned char StartPage,unsigned char column);


void display_shuxian(uint8_t StartPage,uint8_t EndPage,uint8_t column);

void LCD_Init(void);//LCD初始化

void display_map(unsigned char *p);


uint16_t LCD_ReadOutput(void);

//void SetStartPage(uint8_t StartPageAddress);//设置起始页面地址

//void SetStartColumn(uint8_t StartColumnAddress);//设置起始列地址

//void SetStartLine(uint8_t StartLineAddress);//设置起始行地址

//void clrscr(void);//清屏

//void ClrOnePage(unsigned StartPage);//清除一页

//void ClrNumColumn(unsigned char StartPage,unsigned char StartColumn,unsigned Num);//清除StartPage页从StartColumn个汉字位置开始的Num列

//void Displayhz(unsigned char num,const unsigned char *p);//从(0,0)开始显示num个16*16的汉字(包括标点符号)

//void DisplayHz(unsigned char StartPage,unsigned char column,unsigned char Num,const unsigned char *p);//从StartPage页的第Column个汉字位置开始显示Num个汉字

//void DisplayZf(unsigned char StartPage,unsigned char column,unsigned char Num,const unsigned char *p);//从StartPage页的第Column个字符位置开始显示Num个8*16的字符

//void DisplayZF(unsigned char num,const unsigned char *p);//从(0,0)开始显示num个8*16的汉字(包括标点符号)

//void display_map(const unsigned char *p);//显示128*32的图片，p是图片数据首地址

//void LCD_Init();//LCD初始化
#endif
