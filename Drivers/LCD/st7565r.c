#include "stm32f1xx_hal.h"
#include "st7565r_gpio.h"
#include "st7565r.h"
#include "font.h"

#define ASC_CHR_WIDTH	6
#define ASC_CHR_HEIGHT	12
#define ASC_HZ_WIDTH	12

uint16_t LcdStatus = 0;
uint16_t LcdOutput = 0;

void Lcd12232delay(unsigned int Time)//延时函数
{
  unsigned int i,j;
  for ( i=0;i<Time;i++)
		for (j=0;j<5000;j++);
}

void Delay(unsigned int us)
{
  unsigned int i;
  for ( i=0;i<us;i++);
}
uint16_t LCD_ReadOutput(void)
{
	return GPIO_ReadOutputData(GPIOC);
}

uint16_t LCD_ReadStatus(void)
{
	uint16_t status;
	LCD_GPIO_Configuration_read();
	status = GPIO_ReadInputData(GPIOC);
	LCD_GPIO_Configuration();
	return status;
}

void LCD_WriteLByte(uint8_t Byte)//写PC口的低8位，高8位不变
{
  uint16_t Data_PAL;
  Data_PAL = GPIO_ReadOutputData(GPIOC);
  Data_PAL = Data_PAL&0xFF00;//保留高8位数据
  Data_PAL = Data_PAL | Byte;//写入低8位数据
  GPIO_Write(GPIOC,Data_PAL );
}

uint16_t r_status(void)
{
	uint16_t status;
	A0_0;
	CS_0;
	WR_1;
	RD_0;
	Delay(20);
	status = LCD_ReadStatus();
	RD_1;
	CS_1;
	return status;
}
	
void w_com(unsigned char Byte)//向LCD写指令
{
  A0_0;
  CS_0;
  RD_1;
  WR_0;
  Delay(2);
	//LcdOutput = LCD_ReadOutput();
  LCD_WriteLByte(Byte);
  Delay(2);
	//LcdOutput = LCD_ReadOutput();
  WR_1;
	CS_1;
}

void w_data(unsigned char data)//向LCD写数据
{
  A0_1;
  CS_0;
  RD_1;
  WR_0;
  Delay(2);
  LCD_WriteLByte(data);
  Delay(2);
  WR_1;
	CS_1;
}

void SetStartPage(uint8_t StartPageAddress)//设置起始页面地址
{
  w_com(0xB0|StartPageAddress);
}

void SetStartColumn(uint8_t StartColumnAddress)//设置起始列地址
{
	uint8_t lsb = 0;      
	uint8_t msb = 0;      
	lsb = StartColumnAddress & 0x0F;
	msb = StartColumnAddress & 0xF0;
	msb = msb >> 4;  
	msb = msb | 0x10;

  w_com(msb);
	w_com(lsb);
	
	//w_com(0x10|StartColumnAddress);
}
void SetStartLine(uint8_t StartLineAddress)//设置起始行地址
{
  w_com(0x40|StartLineAddress);
}

void clrscr(void)//清屏
{
  uint8_t i,page;
  for(page=0xb0;page<0xb8;page++)
  {
    w_com(page);
    w_com(0x10);//列地址，高低字节两次写入，从第0列开始
		w_com(0x00);
    //w_com(0x40);//行地址，从第0行开始
    for(i=0;i<Max_Column;i++)
    {w_data(0);}
  }
}

void ClrOnePage(uint8_t StartPage)//清除一页
{
  uint8_t i;
  w_com(0xb0+StartPage);//页地址
  w_com(0x10);//列地址，从第0列开始
	w_com(0x00);
  //w_com(0x40);//行地址，从第0行开始
  for(i=0;i<128;i++)
  {w_data(0);}
}

void ClrNumColumn(unsigned char StartPage,unsigned char StartColumn,unsigned char Num)//清除StartPage页从StartColumn个汉字位置开始的Num列
{
  uint8_t i;
#if defined(LCD_REVERSE)
	StartColumn += 4;
#endif
  w_com(0xb0+StartPage);
  SetStartColumn(StartColumn);
  for(i=0;i<Num;i++)
  {w_data(0);}
	w_com(0xb0+StartPage+1);
	SetStartColumn(StartColumn);
	for(i=0;i<Num;i++)
  {w_data(0);}
}

//从StartPage页的第Column个字符位置开始显示Num个8*16的字符
void DisplayZf(unsigned char StartPage,unsigned char column,unsigned char Num,const unsigned char *p)
{
  uint8_t i,j;
  
  SetStartPage(StartPage);
  SetStartColumn(column);//列地址，从第0列开始
  //w_com(0x40);//行地址，从第0行开始
  
  for(i=0;i<Num;i++)
    {
      for(j=0;j<8;j++)
      {w_data(p[16*i+j]);}
    }
  
  SetStartPage(StartPage+1);
  SetStartColumn(column);//列地址，从第0列开始
  //w_com(0x40);//行地址，从第0行开始
  for(i=0;i<Num;i++)
  {
    for(j=0;j<8;j++)
    {w_data(p[16*i+8+j]);}
  }
}

void disp_ch(uint8_t c,uint8_t StartPage,uint8_t column)
{
	uint8_t k,j,ch_w;
	uint16_t len ;

	len=lenASC;
	for(k=0;k<len;k++)
	{
		if(c == ASC_16[k].Index[0] ) break;
	}
	if(k<len)
	{
		  SetStartPage(StartPage);
      SetStartColumn(column);
			for(j=0;j<ASC_CHR_WIDTH;j++)
			{
				ch_w=ASC_16[k].Msk[j];				
				w_data(ch_w); 
			}

			SetStartPage(StartPage+1);
      SetStartColumn(column);
			for(j=0;j<ASC_CHR_WIDTH;j++)
			{
				ch_w=ASC_16[k].Msk[ASC_CHR_WIDTH+j];
				w_data(ch_w); 
			}		
	}
}

void reverse_disp_ch(uint8_t c,uint8_t StartPage,uint8_t column)
{
//	uint8_t k,j,ch_w;
//	for(k=0;k<10;k++)
//	{
//		if(c == reverse_number[k].Index[0] ) break;
//	}
//	if(k<10)
//	{
//		  SetStartPage(StartPage);
//      SetStartColumn(column);
//			for(j=0;j<ASC_CHR_WIDTH;j++)
//			{
//				ch_w=reverse_number[k].Msk[j];				
//				w_data(ch_w); 
//			}

//			SetStartPage(StartPage+1);
//      SetStartColumn(column);
//			for(j=0;j<ASC_CHR_WIDTH;j++)
//			{
//				ch_w=reverse_number[k].Msk[ASC_CHR_WIDTH+j];
//				w_data(ch_w); 
//			}		
//	}
	uint8_t k,j,ch_w;
	uint16_t len ;

	len=lenASC;
	for(k=0;k<len;k++)
	{
		if(c == ASC_16[k].Index[0] ) break;
	}
	if(k<len)
	{
		  SetStartPage(StartPage);
      SetStartColumn(column);
			for(j=0;j<ASC_CHR_WIDTH;j++)
			{
				ch_w=ASC_16[k].Msk[j];				
				w_data(~ch_w); 
			}

			SetStartPage(StartPage+1);
      SetStartColumn(column);
			for(j=0;j<ASC_CHR_WIDTH;j++)
			{
				ch_w=ASC_16[k].Msk[ASC_CHR_WIDTH+j];
				w_data((~ch_w)&0x0F); 
			}		
	}
}

void disp_hz(uint8_t *hz,uint8_t column,uint8_t StartPage)
{
	uint16_t k;
	uint8_t j,ch_w;
	for(k=0;k<lenGB;k++)
	{
		if(hz[0] == GB_16[k].Index[0] && hz[1] == GB_16[k].Index[1] && hz[2] == GB_16[k].Index[2]  )
			break;
	}

	SetStartPage(StartPage);
  SetStartColumn(column);
	for(j=0;j<ASC_HZ_WIDTH;j++)
	{		
		ch_w=GB_16[k].Msk[j];
		w_data(ch_w); 
	}

	SetStartPage(StartPage+1);
  SetStartColumn(column);
	for(j=0;j<ASC_HZ_WIDTH;j++)
	{
		ch_w=GB_16[k].Msk[ASC_HZ_WIDTH+j];
		w_data(ch_w); 
	}
}

void reverse_disp_hz(uint8_t *hz,uint8_t column,uint8_t StartPage)
{
	uint16_t k;
	uint8_t j,ch_w;
	for(k=0;k<lenGB;k++)
	{
		if(hz[0] == GB_16[k].Index[0] && hz[1] == GB_16[k].Index[1] && hz[2] == GB_16[k].Index[2]  )
			break;
	}

	SetStartPage(StartPage);
  SetStartColumn(column);
	for(j=0;j<ASC_HZ_WIDTH;j++)
	{		
		ch_w=GB_16[k].Msk[j];
		w_data(~ch_w); 
	}

	SetStartPage(StartPage+1);
  SetStartColumn(column);
	for(j=0;j<ASC_HZ_WIDTH;j++)
	{
		ch_w=GB_16[k].Msk[ASC_HZ_WIDTH+j];
		w_data((~ch_w)&0x0F); 
	}
}

/*//从StartPage页的第Column个字符位置显示16*16的汉字*/
void display_graphic_12x16(uint8_t *p,uint8_t StartPage,uint8_t column)
{
#if defined(LCD_REVERSE)
	column += 4;
#endif
	unsigned char i=0;
	while(p[i] > 0)
	{
		if(p[i] < 128)
		{	
			disp_ch(p[i],StartPage,column);
			if(p[i] == '.')
				column+=3;
			else 
				column+=6;
			i=i+1;
		}
		else
		{	
			disp_hz(&p[i],column,StartPage);
			column+=12;
			i=i+3;
		}
//		if(Flage1==1) Delayms(200);
		//i=i+3;
	}
	
}

void reverse_display_graphic_12x16(uint8_t *p,uint8_t StartPage,uint8_t column)
{
#if defined(LCD_REVERSE)
	column += 4;
#endif
	unsigned char i=0;
	while(p[i] > 0)
	{
		if(p[i] < 128)
		{	
			reverse_disp_ch(p[i],StartPage,column);
			if(p[i] == '.')
				column+=3;
			else 
				column+=6;
			i=i+1;
		}
		else
		{	
			reverse_disp_hz(&p[i],column,StartPage);
			column+=12;
			i=i+3;
		}
//		if(Flage1==1) Delayms(200);
		//i=i+3;
	}
	
}

void display_shuxian(uint8_t StartPage,uint8_t EndPage,uint8_t column)
{
	#if defined(LCD_REVERSE)
		column+=4;
	#endif
	uint8_t i;
	for(i=StartPage;i<=EndPage;i++)
	{
		SetStartPage(i);
    SetStartColumn(column);
		w_data(0xff);
	}
}


void LCD_Init(void)//LCD初始化
{
   RES_0;
   Lcd12232delay(100);
	 //LcdOutput = LCD_ReadOutput();
   //LcdStatus = r_status();
   RES_1;
   Lcd12232delay(150);
   //w_com(0xE2);		//internal reset
   Lcd12232delay(150);
 /**********lcd inintial************
   w_com(0xa0);    //ADC normal	 
   w_com(0xc8);    //com normal
   w_com(0xa2);    //set partial display duty 1/65
   w_com(0x2c);    //vc on
   w_com(0x2e);    //vr on
   w_com(0x2f);    //internal booster,follower,divided on
   w_com(0x2f);    //internal booster,follower,divided on
   w_com(0x2f);    //internal booster,follower,divided on 
   w_com(0xa6);
   w_com(0x24);    //set rb/ra=5.29
   w_com(0x81);    //set reference voltage select
   w_com(0x20);  
   Lcd12232delay(100);
   w_com(0xaf); 
   w_com(0x40);    //set start line 00 */
#if defined(LCD_REVERSE)
	//***********lcd inintial 3**********
    unsigned char ContrastLevel=0x21;     // 对比度参数初始化设置
    w_com(0xaf);            // 开显示
    w_com(0x40);            // 设置显示起始行=0
    w_com(0xa0);            // RAM列地址与列驱动同顺序
		//w_com(0xa1);          // RAM列地址与列驱动逆顺序  LM6063C
    w_com(0xa6);            // 正向显示
    w_com(0xa4);            // 显示全亮功能关闭
    w_com(0xa2);            // LCD偏压比为1/9
		//w_com(0xc0);            // 行驱动方向为正向 LM6063C
    w_com(0xc8);            // 行驱动方向为反向
    w_com(0x2f);            // 启用内部LCD驱动电源
		w_com(0x25);			// 电阻比
    w_com(0xf8);w_com(0x00);// 倍压设置为4X  
		//w_com(0xf8);w_com(0x01);// 倍压设置为5X     LM6060C
    w_com(0x81);w_com(ContrastLevel); // 设置对比度*/
#else
	//***********lcd inintial 2**********
	unsigned char ContrastLevel=0x23;     // default Contrast Level

	w_com(0xaf);            // display on
	w_com(0x40);            // display start line=0
	w_com(0xa1);            // ADC=1
	w_com(0xa6);            // normal display
	w_com(0xa4);            // Display all point = off
	w_com(0xa2);            // LCD bias = 1/9
	w_com(0xc0);            // Common output mode select= reverse

	w_com(0x2f);            // Power control = all on

	w_com(0x25);			// RA/RB setting

	w_com(0xf8);
	w_com(0x00);// Booster Ratio Set= 2x,3x,4x (2byte command)

	w_com(0x81);            // E-Vol setting
	w_com(ContrastLevel);   // (2byte command) */
#endif

}

void display_map(unsigned char *p)
{
   unsigned char seg;
   unsigned char page;
   for(page=0xb0;page<0xb8;page++)
     {
         w_com(page);
         w_com(0x10);
         w_com(0x04);
         for(seg=0;seg<128;seg++)
            { w_data(*p);
              ++p;
              }
       }
  }

