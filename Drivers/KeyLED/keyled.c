#include "stm32f1xx_hal.h"

volatile uint8_t all_key_up=1;//按键按松开标志	

void KEY_GPIO_Configuration(void)
{	
	//GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitTypeDef GPIO_InitStructure;
  GPIO_InitStructure.Pin = GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15;
  GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_MEDIUM;
	GPIO_InitStructure.Pull = GPIO_PULLUP;
  GPIO_InitStructure.Mode = GPIO_MODE_INPUT;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);
}

void LED_GPIO_Configuration(void)
{	
	//GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitTypeDef GPIO_InitStructure;
  GPIO_InitStructure.Pin = GPIO_PIN_4 | GPIO_PIN_5;
  GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_MEDIUM;//GPIO_Speed_10MHz;
  GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;//GPIO_Mode_Out_PP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

  GPIO_InitStructure.Pin = GPIO_PIN_15;
  GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_MEDIUM;//GPIO_Speed_10MHz;
  GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;//GPIO_Mode_Out_PP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	GPIO_InitStructure.Pin = GPIO_PIN_2;
  GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_MEDIUM;//GPIO_Speed_10MHz;
  GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;//GPIO_Mode_Out_PP;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStructure);
}

void KEY_LED_GPIO_Init(void)
{
	KEY_GPIO_Configuration();
	LED_GPIO_Configuration();
}

GPIO_PinState key_enter_status(void)
{
	return HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_9);
}

GPIO_PinState key_return_status(void)
{
	return HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15);
}
	
GPIO_PinState key_check_status(void)
{
	return HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_13);
}

GPIO_PinState key_up_status(void)
{
	return HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_8);
}

GPIO_PinState key_down_status(void)
{
	return HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14);
}

void led_charge(GPIO_PinState state)
{
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, state);
}

void led_emergency(GPIO_PinState state)
{
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_4, state);
}

void led_fault(GPIO_PinState state)
{
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_5, state);
}

void led_mainpower(GPIO_PinState state)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_15, state);
}

uint8_t KEY_Scan(void)
{	 
	if(all_key_up&&(key_up_status()==0||key_down_status()==0||key_enter_status()==0||key_return_status()==0||key_check_status()==0))
	{
		HAL_Delay (100);//去抖动  
		all_key_up=0;
	 if(key_down_status()==0&&key_enter_status()==0)
		{
			return 6;
		}
		else if(key_up_status()==0)
		{
			return 1;
		}
		else if(key_down_status()==0)
		{ 
			return 2;
		}
		else if(key_enter_status()==0)
		{
			return 3;
		}
		else if(key_return_status()==0)
		{
		
			return 4;
		}
		else if(key_check_status()==0)
		{
			return 5;
		}

	}
	else if(key_up_status()==1&&key_down_status()==1&&key_enter_status()==1&&key_return_status()==1)
		all_key_up=1; 	    
	return 0;// 无按键按下
}


