#include "stm32f1xx_hal.h"

void KEY_LED_GPIO_Init(void);
GPIO_PinState key_enter_status(void);
GPIO_PinState key_return_status(void);
GPIO_PinState key_check_status(void);
GPIO_PinState key_up_status(void);
GPIO_PinState key_down_status(void);
void led_charge(GPIO_PinState state);
void led_emergency(GPIO_PinState state);
void led_fault(GPIO_PinState state);
void led_mainpower(GPIO_PinState state);
uint8_t KEY_Scan(void);

