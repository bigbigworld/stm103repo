
#include "stm32f1xx_hal.h"

UART_HandleTypeDef huart1_232;
UART_HandleTypeDef huart2_485;
/* USART1 init function */
void USART1_232_Init(void)
{

  huart1_232.Instance = USART1;
  huart1_232.Init.BaudRate = 115200;
  huart1_232.Init.WordLength = UART_WORDLENGTH_8B;
  huart1_232.Init.StopBits = UART_STOPBITS_1;
  huart1_232.Init.Parity = UART_PARITY_NONE;
  huart1_232.Init.Mode = UART_MODE_TX_RX;
  huart1_232.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1_232.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1_232) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART2 init function */
void USART2_485_Init(void)
{	
	//485_drv
  GPIO_InitTypeDef GPIO_InitStructure;
  GPIO_InitStructure.Pin = GPIO_PIN_11;
  GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);
	
  huart2_485.Instance = USART2;
  huart2_485.Init.BaudRate = 9600;
  huart2_485.Init.WordLength = UART_WORDLENGTH_8B;
  huart2_485.Init.StopBits = UART_STOPBITS_1;
  huart2_485.Init.Parity = UART_PARITY_NONE;
  huart2_485.Init.Mode = UART_MODE_TX_RX;
  huart2_485.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2_485.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2_485) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

void uart485_state_set(GPIO_PinState state)
{
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, state);
}
