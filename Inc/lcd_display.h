#ifndef _lcd_display_h
#define _lcd_display_h

 typedef struct
{
	uint8_t current;	//?????
	uint8_t  up;//??????
	uint8_t  down;//??????
	uint8_t  enter;//?????
	uint8_t  back;//?????
	uint8_t check;//???????
	uint8_t max_page;//????????????(????????)
	void (*current_operation)(); //??????
} key_table;

void parameter_setting(void);
void factory_setting(void);
void YM_text_option(void);
void year_electric(void);
void month_electric(void);
void user_setting(void);
void main_interface1(void);
void main_interface2(void);
void automatic_YM_text(void);
void factor_password_input(void);
void YM_text_password_input(void);
void history_record(void);
void secret_key_setting(void);
uint8_t isSave(void);
void cell_voltage_display(void);
void cell_voltage_display1(void);
void userPassword_input(void);
void lcd_backligt(GPIO_PinState state);
void lcd_screen_save(void);
void repeat_switch_status(void);

#endif

