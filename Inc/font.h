#ifndef _font_h_
#define _font_h_
typedef struct 
{
    char Index[1];
    char Msk[12];
}typFNT_ASC16;	

typedef struct	
{
	char Index[4];
	char Msk[24];
}typFNT_GB16;

extern unsigned char tab1[];
extern unsigned char tab2[];
extern unsigned char tab3[];
extern const  typFNT_ASC16 reverse_number[];
extern const  typFNT_ASC16  ASC_16[];
extern const typFNT_GB16  GB_16[];
extern unsigned short int lenASC;
extern unsigned short int lenGB;
#endif
