/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2017 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "font.h"
#include "st7565r.h"
#include "bmp.h"
//#include "oled.h"
#include "lcd_display.h"
#include "keyled.h"
#include "timer.h"
#include "usart.h"
#include <stdio.h> 

/* USER CODE BEGIN Includes */
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
extern volatile uint8_t nokey30sec;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);




/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
//**********************************************************
//**********************************************************
struct __FILE {int handle;/* Add whatever you need here */};

FILE __stdout;
FILE __stdin;

int fputc(int ch, FILE *f)
{
  ITM_SendChar(ch);
  return(ch);
}
void (*current_operation_index)();
uint8_t func_index = 0;
uint8_t page =0;
uint8_t f=0;

	 
	key_table table[] =
	{
		{0,3,1,4,0,6,6,(*main_interface1)},
		{1,0,2,4,1,6,6,(*main_interface2)},
		{2,1,16,4,2,6,6,(*cell_voltage_display)},
		{3,16,0,4,3,6,6,(*automatic_YM_text)},
		{4,4,4,5,0,4,4,(*userPassword_input)},
		{5,255,255,255,0,5,10,(*user_setting)},
		{6,6,6,7,0,6,4,(*YM_text_password_input)},
		{7,255,255,255,0,7,2,(*YM_text_option)},
		{8,8,8,8,7,8,6,(*year_electric)},
		{9,9,9,9,7,9,6,(*month_electric)},
		{10,10,10,11,0,10,4,(*factor_password_input)},
		{11,255,255,255,0,11,16,(*factory_setting)},
		{12,255,255,255,11,12,14,(*parameter_setting)},
		{13,13,13,0,0,13,6,(*isSave)},
		{14,14,14,14,5,14,6,(*history_record)},
		{15,15,15,13,13,15,4,(*secret_key_setting)},
		{16,2,3,4,16,6,2,(*cell_voltage_display1)},
		{17,0,0,0,0,0,2,(*lcd_screen_save)},
		{18,18,18,255,5,18,6,(*repeat_switch_status)},
	};
/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */
	uint8_t num;
	
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();
  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  USART1_232_Init();

  /* USER CODE BEGIN 2 */
	USART2_485_Init();
	KEY_LED_GPIO_Init();
	LCD_GPIO_Init();
	LCD_Init();
	TIM_init();
	
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
		clrscr();
		display_map(tab1);
    Lcd12232delay(2000);
    clrscr();
	  display_map(tab2);
		Lcd12232delay(2000);
    clrscr();
		//userPassword_input();
		while(1)
		{
		  num = KEY_Scan();
			if(num)
			{
				switch(num)
				{
					case 1:
					{
						if(table[func_index].up != 255)
						{
							func_index=table[func_index].up;
						}
						else page = (page-2>=0) ? (page-2) : table[func_index].max_page;
						break;
					}

          case 2:
					{
						if(table[func_index].down != 255)
						{
						  func_index=table[func_index].down;
						}
            else page = (page + 2)%(table[func_index].max_page+2);					
						break; 
					}
					
				  case 3:
					{
						if(table[func_index].enter != 255)
						{
							func_index=table[func_index].enter; 
						}
						else f=1;
						break; 
					}
				  case 4:
					{
						page = 0;
				    func_index=table[func_index].back; 
					  break; 
					}
					
					case 5:
					{
						func_index=table[func_index].check;
						break;
					}
					
					case 6:
					{
						func_index=10;
						break;
					}
						
				}
				clrscr();
			}
			else
			{
				if(nokey30sec)
					func_index = 17;
			}
					
			
			current_operation_index=table[func_index].current_operation;
			(*current_operation_index)();
			
		}
		//display_graphic_12x16("主",0,24);
	  //display_graphic_12x16(":1",0,12);
		//fun18();
		//Lcd12232delay(1000);
    //clrscr();

		
		//if(GPIO_PIN_SET == key_enter_status())
		//	led_emergency(GPIO_PIN_SET);
  /* USER CODE END WHILE */
		

  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}



/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
