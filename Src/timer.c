
/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "timer.h"
#include "st7565r.h"
#include "usart.h"

extern UART_HandleTypeDef huart1_232;
extern UART_HandleTypeDef huart2_485;
extern volatile uint8_t all_key_up;
/** @addtogroup STM32F1xx_HAL_Examples
  * @{
  */

/** @addtogroup TIM_TimeBase
  * @{
  */

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
volatile uint8_t nokey30sec;
/* TIM handle declaration */
TIM_HandleTypeDef    TimHandle;

/* Prescaler declaration */
uint32_t uwPrescalerValue = 0;

/* Private function prototypes -----------------------------------------------*/


/* Private functions ---------------------------------------------------------*/

void TIM_init(void)
{
/*##-1- Configure the TIM peripheral #######################################*/
  /* -----------------------------------------------------------------------
    In this example TIM3 input clock (TIM3CLK)  is set to APB1 clock (PCLK1) x2,
    since APB1 prescaler is set to 4 (0x100).
       TIM3CLK = PCLK1*2
       PCLK1   = HCLK/2
    => TIM3CLK = PCLK1*2 = (HCLK/2)*2 = HCLK = SystemCoreClock
    To get TIM3 counter clock at 10 KHz, the Prescaler is computed as following:
    Prescaler = (TIM3CLK / TIM3 counter clock) - 1
    Prescaler = (SystemCoreClock /10 KHz) - 1

    Note:
     SystemCoreClock variable holds HCLK frequency and is defined in system_stm32f1xx.c file.
     Each time the core clock (HCLK) changes, user had to update SystemCoreClock
     variable value. Otherwise, any configuration based on this variable will be incorrect.
     This variable is updated in three ways:
      1) by calling CMSIS function SystemCoreClockUpdate()
      2) by calling HAL API function HAL_RCC_GetSysClockFreq()
      3) each time HAL_RCC_ClockConfig() is called to configure the system clock frequency
  ----------------------------------------------------------------------- */

  /* Compute the prescaler value to have TIMx counter clock equal to 10000 Hz */
  uwPrescalerValue = (uint32_t)(SystemCoreClock / 50000) - 1;

  /* Set TIMx instance */
  TimHandle.Instance = TIMx;

  /* Initialize TIMx peripheral as follows:
       + Period = 10000 - 1
       + Prescaler = (SystemCoreClock/10000) - 1
       + ClockDivision = 0
       + Counter direction = Up
  */
  TimHandle.Init.Period            = 25- 1;
  TimHandle.Init.Prescaler         = uwPrescalerValue;
  TimHandle.Init.ClockDivision     = 0;
  TimHandle.Init.CounterMode       = TIM_COUNTERMODE_UP;
  TimHandle.Init.RepetitionCounter = 0;
  TimHandle.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;

  if (HAL_TIM_Base_Init(&TimHandle) != HAL_OK)
  {
    /* Initialization Error */
    while (1)
		{
		}
  }

  /*##-2- Start the TIM Base generation in interrupt mode ####################*/
  /* Start Channel1 */
  if (HAL_TIM_Base_Start_IT(&TimHandle) != HAL_OK)
  {
    /* Starting Error */
		while (1)
		{
		}
  }
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  //add function for timer here, 500 us every time
	uint8_t ucTxTmp;
	uint8_t ucRxTmp;
	static uint32_t count30sec = 0;
	if(all_key_up)
	{
		count30sec++;
		if(count30sec >= 60000)
		{
			count30sec = 0;
			nokey30sec = 1;
		}
	}
	else
	{
		count30sec = 0;	
		nokey30sec = 0;
	}
	uart485_state_set(GPIO_PIN_SET);
	if(HAL_UART_GetState(&huart2_485) == HAL_UART_STATE_READY || HAL_UART_GetState(&huart2_485) == HAL_UART_STATE_BUSY_RX)
	{
		//add here application function
		HAL_UART_Transmit(&huart2_485, &ucTxTmp, 1, 0);
	}

	uart485_state_set(GPIO_PIN_RESET);
	if(HAL_UART_GetState(&huart2_485) == HAL_UART_STATE_READY || HAL_UART_GetState(&huart2_485) == HAL_UART_STATE_BUSY_TX)
	{
		HAL_UART_Receive(&huart2_485, &ucRxTmp, 1, 0); 
		//add here application function
	}

}

