#include "stm32f1xx_hal.h"
#include "st7565r.h"
#include "font.h"
#include "keyled.h"
#include "string.h"
#include "lcd_display.h"
//#include "Global.h"

extern uint8_t page;
extern int8_t func_index;
extern uint8_t f;
extern key_table table[];
extern volatile uint8_t nokey30sec;

void delay_with_KEY_Scan(uint8_t *times)
{
	uint8_t num;
	unsigned int i,j;
  for ( i=0;i<15;i++)
		for (j=0;j<5000;j++)
		{
			num = KEY_Scan();
			if(num)
			{
				switch(num)
				{
					case 4:(*times)++;return;
				}
			}	
		}
}

void delay_main2(uint8_t *flag)
{
		uint8_t num;
	unsigned int i,j;
  for ( i=0;i<20;i++)
		for (j=0;j<5000;j++)
		{
			num = KEY_Scan();
			if(num)
			{
				switch(num)
				{
					case 1:*flag=1;func_index=table[func_index].up;return;
					case 2:*flag=1;func_index=table[func_index].down;return;
					case 3:*flag=1;func_index=table[func_index].enter;return;
					case 5:*flag=1;func_index=table[func_index].check;return;
					case 6:*flag=1;func_index=10;return;
				}
			}
			else if(nokey30sec)
				return;
		}
}

void delay_repeat_switch(uint8_t *flag)
{
	uint8_t num;
	unsigned int i,j;
  for ( i=0;i<18;i++)
		for (j=0;j<5000;j++)
		{
			num = KEY_Scan();
			if(num==3)
			{
				f = 1;
				*flag = 1;
				return;
			}
			else if(num==4)
			{
				*flag=1;
				func_index = table[func_index].back;
				return;
			}
		}
}

uint8_t * int_str(uint16_t data,uint8_t str[])
{
	uint8_t i,j=2,tem;
	if(data==0)
		strcpy(str,"000");
	else 
		for(i=0;i<3;i++)
	  {
			tem = data%10;
			str[j--] = tem+0x30;
			data = data/10;
		}
		str[3]='\0';
		return str;
}

uint8_t * float_str(uint16_t data,uint8_t str[])
{
	uint8_t i,j=2,tem;
	if(data==0)
		strcpy(str,"00.0");
	else 
	{
		for(i=0;i<3;i++)
	  {
			tem = data%10;
			str[j--] = tem+0x30;
			data = data/10;
		}
		str[3]=str[2];
		str[2]='.';

	}
		return str;
}

int_least8_t signed_int_str(uint8_t par,uint8_t isfloat,uint8_t str[])
{
	uint8_t str1[4];
	if((par & 0x80) == 0x80)
	{
		par = par ^ 0xff;
		if(isfloat)
		{
			float_str(par,str1);
		}
		else
		{
			int_str(par,str1);
		}
		sprintf(str,"%s%.4s","-",str1);
		return 0-par;
	}
	else
	{
		if(isfloat)
		{
			float_str(par,str1);
		}
		else
		{
			int_str(par,str1);
		}
		sprintf(str,"%s%.4s"," ",str1);
		return par;
	}
}

/*是否保存*/
uint8_t isSave(void)
{
	uint8_t num,flag=0;
	clrscr ();
	display_graphic_12x16("是否保存参数修改",0,12);
	display_graphic_12x16("保存->Enter",2,24);
	display_graphic_12x16("取消->Esc",4,24);
	while(1)
	{
		num = KEY_Scan();
		if(num)
		{
			if(flag==1)
			{
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);
				return 0;
			}
			switch(num)
			{
				case 3:
				{
					reverse_display_graphic_12x16("保存->Enter",2,24);
					HAL_Delay(500);
					clrscr();
					return 1;
				}
				case 1:
				case 2:
				case 4:
				{
					reverse_display_graphic_12x16("取消->Esc",4,24);
					HAL_Delay(500);
					clrscr();
					return 0;
				}
			}
		}
		else if(nokey30sec)
		{
			lcd_screen_save();
			flag=1;
		}
			
	}
}

/*密码输入*/
void password_input(uint8_t *password,uint8_t index)
{
	uint8_t j,i=0;
	uint8_t column=60,column1=60;
	uint8_t num;
	uint8_t input_passwd[3]="000";
	
	#if defined(LCD_REVERSE)
		column = column + 4;
		column1 = column1 +4;
	#endif
	
	display_graphic_12x16("密码输入:",4,0);
	display_graphic_12x16(input_passwd,4,60);
	
	while(1)
	{	
	num = KEY_Scan();
	if(num)
	{
				switch(num)
				{
					case 1:
					{
						input_passwd[i] = (char)(input_passwd[i]+1);
						if(input_passwd[i] > '9')
								input_passwd[i] = '0';
						break;
					}

          case 2:
					{
						input_passwd[i] = (char)(input_passwd[i]-1);
						if(input_passwd[i] < '0')
								input_passwd[i] = '9';						
						break; 
					}
					
				  case 3:
					{
						i++;
						column = column+6;
						if(i==3)
						{
							if(strcmp(input_passwd,password) == 0)
							{
								func_index = index;
								clrscr();
								return;
							}
							else
							{
								display_graphic_12x16 ("密码错误",6,60);
								strcpy(input_passwd ,"000");
							}
							i = 0;
							column = column1;
						}
            break; 
					}
				  case 4:
					{
						func_index = 0;
						clrscr ();
						return;
					}
						
				}
	}
	else if(nokey30sec)
		return;
	for(j=0;j<3;j++)
	{
		if(i==j)
			reverse_disp_ch(input_passwd[j],4,column);
		else
			disp_ch(input_passwd[j],4,column1+j*6);
	}
}
	
}
uint8_t choose_parameter(uint8_t index,uint8_t max,uint8_t column,uint8_t **p)
{
	uint8_t i=index;
	uint8_t num,flag=0;
	while(1)
			{
				num = KEY_Scan();
				if(num)
				{
					if(flag==1)
			    {
						HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);
						return index;
					}
					switch(num)
					{
						case 1: i = (i+1)%max;break;
						case 2: i = (i-1>=0)? i-1 : max-1;break;
						case 3: index = (isSave() == 1) ? i : index;return index;
						case 4: index = (isSave() == 1) ? i : index; return index;
					}
					ClrNumColumn(page,column,60);
				}
				else if(nokey30sec)
		    {
					lcd_screen_save();
					flag=1;
					continue;
		    }
				reverse_display_graphic_12x16(p[i],page,column);
			}
}
uint8_t change_parameter(uint8_t par[],uint8_t column,uint8_t n)
{
	#if defined(LCD_REVERSE)
		column = column + 4;
	#endif
	
	uint8_t num,i=0,j,page1,flag=0;
	uint8_t col = column;
	uint8_t col1 =column;
	if(page<=6)
		page1 = page;
	else page1 = page-8;
	while(1)
	{
		num = KEY_Scan();
		if(num)
		{
			if(flag==1)
			{
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);
				return 0;
			}
			switch(num)
			{
				case 1:
				{
					par[i] = (char)(par[i]+1);
					if(par[i] > '9')
							par[i] = '0';
					break;
				}
				case 2:
				{
					par[i] = (char)(par[i]-1);
					if(par[i] < '0')
							par[i] = '9';						
					 break; 
				}
				case 3:
				{
					i++;
					col1 = col1+6;
					if(i==n)
					{
						clrscr();
						return(isSave());
					}
					break;
				}
				case 4:
				{
					clrscr();
					return(isSave());
				}
				
			}
		}
		else if(nokey30sec)
		{
			lcd_screen_save();
			flag=1;
			continue;
		}
		if(par[i]== '.')
		{
			i++;
			col1=col1+3;
		}
		for(j=0;j<n;j++)
		{
			if(i==j)
				reverse_disp_ch(par[j],page1,col1);
			else
				disp_ch(par[j],page1,col);
			if(par[j]=='.')
				col=col+3;
			else
				col=col+6;
		}
		col = column;
	}
}

void change_parameter_with_singed(int_least8_t *par,uint8_t str_par[],uint8_t column,uint8_t isfloat)
{
	#if defined(LCD_REVERSE)
		column = column + 4;
	#endif
	int_least8_t tem = *par;
	uint8_t str[4],str1[5];
	uint8_t num,page1,flag=0,flag_ischange=1;
	if(page<=6)
		page1 = page;
	else page1 = page-8;
	while(1)
	{
		num = KEY_Scan();
		if(num)
		{
			if(flag==1)
			{
				HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, GPIO_PIN_SET);
				return;
			}
			switch(num)
			{
				case 1:
				{
					flag_ischange=1;
					tem=tem+1;
					break;
				}
				case 2:
				{
					flag_ischange=1;
					 tem=tem-1;					
					 break; 
				}
				case 3:
				{
					clrscr();
					if(isSave())
					{
						*par=tem;
						if(tem>=0)
							sprintf(str_par,"%.1s%s"," ",str1);
						else
							sprintf(str_par,"%.1s%s","-",str1);
					}
					return;
				}
				case 4:
				{
					clrscr();
					if(isSave())
					{
						*par = tem;
						if(tem>=0)
							sprintf(str_par,"%.1s%s"," ",str1);
						else
							sprintf(str_par,"%.1s%s","-",str1);
					}
					return;
				}
				
			}
		}
		else if(nokey30sec)
		{
			lcd_screen_save();
			flag=1;
			continue;
		}
		if(tem>=0)
		{
			if(isfloat)
			{
				float_str(tem,str1);
			}
			else
			{
				int_str(tem,str1);
			}
			if(flag_ischange)
			{
				display_graphic_12x16(" ",page1,column);
				reverse_display_graphic_12x16(str1,page1,column+6);
				flag_ischange=0;
			}
		}
		else
		{
			if(isfloat)
			{
				float_str(0-tem,str1);
			}
			else
			{
				int_str(0-tem,str1);
			}
			if(flag_ischange)
			{
				display_graphic_12x16("-",page1,column);
				reverse_display_graphic_12x16(str1,page1,column+6);
				flag_ischange=0;
			}
	  }
	}
}

/*参数微调*/
uint8_t main_v[]=" 000";
uint8_t out_v[]=" 000";
uint8_t charge_current[]=" 00.0";
uint8_t cell_voltage[]=" 00.0";
uint8_t A_out[]=" 000";
uint8_t B_out[]=" 000";
uint8_t C_out[]=" 000";
uint8_t single_voltage[]=" 00.0";
uint8_t u_main_v=0xfd,u_out_v=0,u_charge_current=0xfe,u_cell_voltage=0,u_A_out=0,u_B_out=0,u_C_out=0,u_single_voltage=0;
int_least8_t int_main_v = 0, int_out_v=0,int_charge_current=0,int_cell_voltage=0,int_A_out=0, int_B_out=0,int_C_out=0,int_single_voltage=0;
uint8_t flag_first=1;
void parameter_setting(void)
{
	uint8_t str[5];
	if(flag_first==1)
	{
		int_main_v=signed_int_str(u_main_v,0,main_v);
		int_out_v=signed_int_str(u_out_v,0,out_v);
		int_charge_current=signed_int_str(u_charge_current,1,charge_current);
		int_cell_voltage=signed_int_str(u_cell_voltage,1,cell_voltage);
		int_A_out=signed_int_str(u_A_out,0,A_out);
		int_B_out=signed_int_str(u_B_out,0,B_out);
		int_C_out=signed_int_str(u_C_out,0,C_out);
		int_single_voltage=signed_int_str(u_single_voltage,1,single_voltage);
		flag_first=0;
		
	}
	if(page<=6)
	{
		display_graphic_12x16 ("》",page,6);
		display_graphic_12x16("主电电压:",0,18);
		display_graphic_12x16(main_v,0,90);
		display_graphic_12x16("输出电压:",2,18);
		display_graphic_12x16(out_v,2,90);
		display_graphic_12x16("充电电流:",4,18);
		display_graphic_12x16(charge_current,4,90);
		display_graphic_12x16("电池电压:",6,18);
		display_graphic_12x16(cell_voltage,6,90);
	}
	else
	{
		display_graphic_12x16 ("》",page-8,0);
		display_graphic_12x16("A相输出电流:",0,12);
		display_graphic_12x16(A_out,0,96);
		display_graphic_12x16("B相输出电流:",2,12);
		display_graphic_12x16(B_out,2,96);
		display_graphic_12x16("C相输出电流:",4,12);
		display_graphic_12x16(C_out,4,96);
		display_graphic_12x16("单只电池电压:",6,12);
		display_graphic_12x16(single_voltage,6,96);
	}
	if(f==1)
	{
		f=0;
		if(page==0)
		{
			change_parameter_with_singed(&int_main_v,main_v,84,0);
		}
		if(page==2)
		{
			change_parameter_with_singed(&int_out_v,out_v,84,0);
		}
		if(page==4)
		{
			change_parameter_with_singed(&int_charge_current,charge_current,84,1);
		}
		if(page==6)
		{
			change_parameter_with_singed(&int_cell_voltage,cell_voltage,84,1);
		}
		if(page==8)
		{
        change_parameter_with_singed(&int_A_out,A_out,90,0);
		}
		if(page==10)
		{
			change_parameter_with_singed(&int_B_out,B_out,90,0);
		}
		if(page==12)
		{
			change_parameter_with_singed(&int_C_out,C_out,90,0);

		}
		if(page==14)
		{
			change_parameter_with_singed(&int_single_voltage,single_voltage,90,1);
		}
	}
	
}

/*工厂密码输入*/
uint8_t factor_password[] = "123";
void factor_password_input(void)
{
	display_graphic_12x16("工厂设置",0,40);
	password_input(factor_password,11);
}

/*工厂设置*/
uint8_t *power[] = {"1KW","1.5KW","2KW","2.5KW","3KW","4KW","5KW","6KW","8KW","10KW"};
uint8_t *fault_switch[] = {"声光告警","声告警","光告警"};
uint8_t *polling[] = {"开","关"};
uint8_t overvoltage[] = "250";//主电过压门限
uint8_t main_undervoltage[] = "160";//主电欠压门限
uint8_t output_overload[] = "150";//输出过载门限
uint8_t low_voltage[] = "80";//电池欠压门限
void factory_setting(void)
{
	static uint8_t index_power=0,index_fault=2,index_polling=0;
	uint8_t i=index_power,j=index_fault;
	uint8_t c;
	uint8_t num;
	uint8_t temp[4];
	if(page<=6)
	{
		display_graphic_12x16 ("》",page,0);
		
		display_graphic_12x16("功率选择:",0,12);
		display_graphic_12x16(power[index_power],0,64);
		display_graphic_12x16("电池巡检:",2,12);
		display_graphic_12x16(polling[index_polling],2,64);
		display_graphic_12x16("声光开关:",4,12);
		display_graphic_12x16(fault_switch[index_fault],4,64);
		display_graphic_12x16("主电过压门限:",6,12);
		display_graphic_12x16(overvoltage,6,96);
		display_graphic_12x16("V",6,114);
	}
	else if(page<=14)
	{
		display_graphic_12x16 ("》",page-8,0);
		display_graphic_12x16("主电欠压门限:",0,12);
		display_graphic_12x16(main_undervoltage,0,96);
		display_graphic_12x16("V",0,114);
		display_graphic_12x16("输出过载门限:",2,12);
		display_graphic_12x16(output_overload ,2,96);
		display_graphic_12x16("%",2,114);
		display_graphic_12x16("电池欠压门限:",4,12);
		display_graphic_12x16(low_voltage,4,96);
		display_graphic_12x16("%",4,108);
		display_graphic_12x16("请输入新密码:",6,12);
		display_graphic_12x16(factor_password,6,96);
		
	}
	else 
	{
		display_graphic_12x16 ("》",page-16,0);
		display_graphic_12x16("参数微调",0,12);
	}
	if(f==1)
	{
		f=0;
		if(page == 0)
			index_power = choose_parameter(index_power,10,64,power);
		if(page==2)
		{
			index_polling = choose_parameter(index_polling,2,64,polling);
			if(index_polling == 1)
			{
				table[1].down = 3;
				table[3].up = 1;
			}
			
			else
			{
				table[1].down = 2;
				table[3].up = 2;
			}
		}			
		if(page==4)
		{
			while(1)
			{
				num = KEY_Scan();
				if(num)
				{
					switch(num)
					{
						case 1: j = (j+1)%3;break;
						case 2: j = (j-1>=0)? j-1 : 2;break;
						case 3: index_fault = (isSave() == 1) ? j : index_fault; return;
						case 4: index_fault = (isSave() == 1) ? j : index_fault; return;
					}
					ClrNumColumn(4,64,50);
				}
				else if(nokey30sec)
					return;
				reverse_display_graphic_12x16(fault_switch[j],4,64);
			}
		}
		if(page==6)
		{
			strcpy(temp,overvoltage);
			if(change_parameter(temp,96,3)==1)
				strcpy(overvoltage,temp);
		}
		if(page==8)
		{
			strcpy(temp,main_undervoltage);
			if(change_parameter(temp,96,3)==1)
				strcpy(main_undervoltage ,temp);
		}
		if(page==10)
		{
			strcpy(temp,output_overload);
			if(change_parameter(temp,96,3)==1)
				strcpy(output_overload ,temp);
		}
		if(page==12)
		{
			strcpy(temp,low_voltage);
			if(change_parameter(temp,96,2)==1)
				strcpy(low_voltage ,temp);
		}
		if(page==14)
		{
			strcpy(temp,factor_password);
			if(change_parameter(temp,96,3)==1)
				strcpy(factor_password ,temp);
		}
		if(page == 16)
		{
			func_index = 12;
			clrscr();
			page = 0;
			return;
		}


	}
	
}


/*年检月检1*/
void YM_text_option(void)
{
	if(f==1)
	{
		f=0;
		if(page==0)
			func_index = 8;
		if(page==2)
			func_index = 9;
		clrscr();
		return;
	}
	display_graphic_12x16("》",page+2,24);
	display_graphic_12x16("年检月检选择",0,24);
	display_graphic_12x16("手动年检",2,36);
	display_graphic_12x16("手动月检",4,36);
}

/*年检月检2*/
uint16_t year_time=0,year_main=235,year_out_main=225,year_voltage=481,year_out_voltage=0;
void year_electric(void)
{
	uint8_t str[4];
	display_graphic_12x16("手动年放电",0,30);
	display_graphic_12x16("正在放电",2,12);
	display_graphic_12x16("已放   m",2,67);
	display_graphic_12x16(int_str(year_time,str),2,91);
	display_graphic_12x16("主电:   V",4,0);
	display_graphic_12x16(int_str(year_main,str),4,30);
	display_graphic_12x16("输出:   V",4,66);
	display_graphic_12x16(int_str(year_out_main,str),4,96);
	display_graphic_12x16("电池:  . V",6,0);
	display_graphic_12x16(float_str(year_voltage,str),6,30);
	display_graphic_12x16("输出:  . A",6,66);
	display_graphic_12x16(float_str(year_out_voltage,str),6,96);
	
}
uint16_t month_time=0,month_main=235,month_out_main=225,month_voltage=481,month_out_voltage=0;
void month_electric(void)
{
	uint8_t str[4];
	display_graphic_12x16("手动月放电",0,30);
	display_graphic_12x16("正在放电",2,12);
	display_graphic_12x16("已放   m",2,67);
	display_graphic_12x16(int_str(month_time,str),2,91);
	display_graphic_12x16("主电:   V",4,0);
	display_graphic_12x16(int_str(month_main,str),4,30);
	display_graphic_12x16("输出:   V",4,66);
	display_graphic_12x16(int_str(month_out_main,str),4,96);
	display_graphic_12x16("电池:  . V",6,0);
	display_graphic_12x16(float_str(month_voltage,str),6,30);
	display_graphic_12x16("输出:  . A",6,66);
	display_graphic_12x16(float_str(month_out_voltage,str),6,96);
	
}


/*主界面1*/
void main_interface1(void)
{
	uint16_t v =12000;
	uint16_t out_v=v/100,out_A=0,in_A=10000/100,in_B=100,in_C=10,ch_A=110,va_V=110;
	uint8_t str[4];
	lcd_backligt(GPIO_PIN_SET);
	display_graphic_12x16("输入",0,6);
	display_graphic_12x16("输出电压:   V",0,46);
	display_graphic_12x16(int_str(out_v,str),0,100);
	display_graphic_12x16("A:   V",2,0);
	display_graphic_12x16(int_str(in_A,str),2,12);
	display_graphic_12x16("输出电流:  . A",2,46);
	display_graphic_12x16(float_str(out_A,str),2,100);
	display_graphic_12x16("B:   V",4,0);
	display_graphic_12x16(int_str(in_B,str),4,12);
	display_graphic_12x16("充电电流:  . A",4,46);
	display_graphic_12x16(float_str(ch_A,str),4,100);
	display_graphic_12x16("C:   V",6,0);
	display_graphic_12x16(int_str(in_C,str),6,12);
	display_graphic_12x16("电池电压:  . V",6,46);
	display_graphic_12x16(float_str(va_V,str),6,100);
	display_shuxian(0,7,40);
}

/*主界面2*/
uint8_t history1[5][20]={"\0","\0","\0","\0","\0"};
uint8_t history2[18][2];
void main_interface2(void)
{
	uint8_t i,battery_num[] = "00";
	uint8_t flag=0,j=0;
	uint8_t charger_open=0,batter_low_vor=0,branch_open=0,main_power_falt=0,output_over_load=1,battery_low_vor1=0xff,battery_low_vor2=1,battery_low_vor3=0;
	display_graphic_12x16("工作状态:",0,6);
	display_graphic_12x16("应急输出",0,66);
	display_graphic_12x16("故障类型:",2,6);

	display_graphic_12x16("电池状态:",4,6);
	display_graphic_12x16("电池放电",4,66);
	display_graphic_12x16("控制类型:",6,6);
	display_graphic_12x16("自动应急",6,66);

	if(charger_open || batter_low_vor || branch_open || main_power_falt || output_over_load || (battery_low_vor1&0xC0) || battery_low_vor2 || battery_low_vor3 )
	{
		while(1)
		{
			if(charger_open)
			{
				strcpy(history1[0],"充电器开路");
				display_graphic_12x16("充电器开路",2,66);
				delay_main2(&flag);
				if(flag==1) 
				{clrscr();return;}
				ClrNumColumn(2,66,61);
			}
			if(batter_low_vor)
			{
				strcpy(history1[1],"电池欠压");
				display_graphic_12x16("电池欠压",2,66);
				delay_main2(&flag);
				if(flag==1) 
				{clrscr();return;}
				ClrNumColumn(2,66,61);
			}
			if(branch_open)
			{
				strcpy(history1[2],"支路开路");
				display_graphic_12x16("支路开路",2,66);
				delay_main2(&flag);
				if(flag==1) 
				{clrscr();return;}
				ClrNumColumn(2,66,61);
			}
			if(main_power_falt)
			{
				strcpy(history1[3],"主电故障");
				display_graphic_12x16("主电故障",2,66);
				delay_main2(&flag);
				if(flag==1) 
				{clrscr();return;}
				ClrNumColumn(2,66,61);
			}
			if(output_over_load)
			{
				strcpy(history1[4],"输出过载");
				display_graphic_12x16("输出过载",2,66);
				delay_main2(&flag);
				if(flag==1) 
				{clrscr();return;}
				ClrNumColumn(2,66,61);
			}
			if(battery_low_vor2)
			{
				for(i=0; i<8; i++)
				{
					if(battery_low_vor2 & (1<<i))
					{
						battery_num[1] = 0x30+i+1;
						strcpy(history2[i],battery_num);
						display_graphic_12x16("电池",2,66);
						display_graphic_12x16(battery_num,2,90);
						display_graphic_12x16("欠压",2,102);
						delay_main2(&flag);
						if(flag==1) 
						{clrscr();return;}
						ClrNumColumn(2,66,61);
					}
				}
			}
			if(battery_low_vor3)//battery09~16
			{
				for(i=0; i<8; i++)
				{
					if(battery_low_vor2 & (1<<i))
					{
						if(i == 0)//battery09
						{
							battery_num[1] = 0x30+i+9;
							strcpy(history2[8+i],battery_num);
							display_graphic_12x16("电池",2,66);
							display_graphic_12x16(battery_num,2,90);
							display_graphic_12x16("欠压",2,102);
							delay_main2(&flag);
							if(flag==1) 
							{clrscr();return;}
							ClrNumColumn(2,66,61);
						}
						else
						{
							battery_num[0] = '1';
							battery_num[1] = 0x30+i-1;
							strcpy(history2[8+i],battery_num);
							display_graphic_12x16("电池",2,66);
							display_graphic_12x16(battery_num,2,90);
							display_graphic_12x16("欠压",2,102);
							delay_main2(&flag);
							if(flag==1) 
					    {clrscr();return;}
							ClrNumColumn(2,66,61);
						}
					}
				}
			}
			if(battery_low_vor1 & 0xC0)//battery17~18
			{
				for(i=6; i<8; i++)
				{
					battery_num[0] = '1';
					battery_num[1] = 0x30+i+1;
					strcpy(history2[16+i-6],battery_num);
					display_graphic_12x16("电池",2,66);
					display_graphic_12x16(battery_num,2,90);
					display_graphic_12x16("欠压",2,102);
					delay_main2(&flag);
					if(flag==1) 
					{clrscr();return;}
					ClrNumColumn(2,66,61);
				}
			}
			
			break;
		}
	}
	else
	{
		display_graphic_12x16("无故障",2,66);
	}
	
}

/*主界面4*/
void automatic_YM_text(void)
{
	uint16_t m_times=0,y_times=0;
	uint8_t num;
	uint8_t times=0;
	uint8_t str[2];
	static uint8_t month_countdown=1,year_countdown=1;
	uint8_t strMonth[2];
	uint8_t strYear[2];
	sprintf(strMonth,"%02d",month_countdown);
	sprintf(strYear,"%02d",year_countdown);
	display_graphic_12x16("自动月放电倒计时:  天",0,0);
	display_graphic_12x16(strMonth,0,102);
	sprintf(str,"%02d",m_times);
	display_graphic_12x16("已放  次",2,24);
	display_graphic_12x16(str,2,48);
	display_graphic_12x16("放电完毕",2,80);
	display_graphic_12x16("自动年放电倒计时:  月",4,0);
	display_graphic_12x16(strYear,4,102);
	sprintf(str,"%02d",y_times);
	display_graphic_12x16("已放  次",6,24);
	display_graphic_12x16(str,6,48);
	while(1)
	{
		num = KEY_Scan();
		if(num==0)
			if(nokey30sec)
				return;
		if(num==4)
			times++;
		if(num==0)
			num=4;
		switch(num)
		{
			case 4:
			{
				if(times==1)
				{
					month_countdown=(month_countdown+1)%32;
					if(month_countdown==0)
						month_countdown++;	
					ClrNumColumn(0,102,12);
					sprintf(strMonth,"%02d",month_countdown);
					display_graphic_12x16(strMonth,0,102);
					delay_with_KEY_Scan(&times);
				}
				if(times==2)
				{
					year_countdown=(year_countdown+1)%13;
					if(year_countdown==0)
						year_countdown++;	
					ClrNumColumn(4,102,12);
					sprintf(strYear,"%02d",year_countdown);
					display_graphic_12x16(strYear,4,102);
					delay_with_KEY_Scan(&times);
				}
				break;
			}
			case 1:func_index=table[func_index].up;clrscr();return;
			case 2:func_index=table[func_index].down;clrscr();return;
			case 3:func_index=table[func_index].enter;clrscr();return;
			case 5:func_index=table[func_index].check;clrscr();return;
			case 6:func_index=10;clrscr();return;
		}
	}

}


/*年检月检0*/
uint8_t *YM_text_password="123";
void YM_text_password_input(void)
{
	display_graphic_12x16("年检月检密码",0,28);
	password_input(YM_text_password,7);
}

/*历史记录*/
void history_record(void)
{
	uint8_t i,j=1;
	display_graphic_12x16("历史记录",0,40);
	for(i=0;i<5;i++)
	{
		if(history1[i][0]!=0x00)
		{
			switch(j)
			{
				case 1:
				{
					j++;
					display_graphic_12x16("1 ",2,0);
					display_graphic_12x16(history1[i],2,12);
					break;
				}
				case 2:
				{
					j++;
					display_graphic_12x16("2 ",2,74);
					display_graphic_12x16(history1[i],2,86);
					break;
				}
				case 3:
				{
					j++;
					display_graphic_12x16("3 ",4,0);
					display_graphic_12x16(history1[i],4,12);
					break;
				}
				case 4:
				{
					j++;
					display_graphic_12x16("4 ",4,74);
					display_graphic_12x16(history1[i],4,86);
					break;
				}
				
			}
			
		}

	}
	
  for(i=0;i<18;i++)
	{
		if(history2[i][0]!='\0')
		{
			switch(j)
			{
				case 1:
				{
					j++;
					display_graphic_12x16("1 ",2,0);
					display_graphic_12x16(history2[i],2,12);
					display_graphic_12x16("欠压",2,24);
					break;
				}
				case 2:
				{
					j++;
					display_graphic_12x16("2 ",2,74);
					display_graphic_12x16(history2[i],2,86);
					display_graphic_12x16("欠压",2,98);
					break;
				}
				case 3:
				{
					j++;
					display_graphic_12x16("3 ",4,0);
					display_graphic_12x16(history2[i],4,12);
					display_graphic_12x16("欠压",4,24);
					break;
				}
				case 4:
				{
					j++;
					display_graphic_12x16("4 ",4,74);
					display_graphic_12x16(history2[i],4,86);
					display_graphic_12x16("欠压",4,98);
					break;
				}
				case 5:
				{
					j++;
					display_graphic_12x16("5",6,0);
					display_graphic_12x16(history2[i],6,12);
					display_graphic_12x16("欠压",6,24);
					break;
				}
				case 6:
				{
					j++;
					display_graphic_12x16("6",6,74);
					display_graphic_12x16(history2[i],6,86);
					display_graphic_12x16("欠压",6,98);
					break;
				}
			}
			
		}

	}
}

/*用户密钥*/

void secret_key_setting(void)
{
	display_graphic_12x16("用户密钥",0,40);
	display_graphic_12x16("请输入密钥:",2,6);
	display_graphic_12x16("00000000",4,60);
	
}


/*主界面3-单体电池电压显示*/
void cell_voltage_display(void)
{
	uint8_t str[3];
	uint16_t v1=124,v2=0,v3=0,v4=0,v5=0,v6=0,v7=0,v8=0,v9=0,v10=0,v11=0,v12=0;
	display_graphic_12x16("01#  .  02#  .  03#  . ",0,0);
	display_graphic_12x16(float_str(v1,str),0,18);
	display_graphic_12x16(float_str(v2,str),0,64);
	display_graphic_12x16(float_str(v3,str),0,108);
	display_graphic_12x16("04#  .  05#  .  06#  . ",2,0);
	display_graphic_12x16(float_str(v4,str),2,18);
	display_graphic_12x16(float_str(v5,str),2,64);
	display_graphic_12x16(float_str(v6,str),2,108);
	display_graphic_12x16("07#  .  08#  .  09#  . ",4,0);
	display_graphic_12x16(float_str(v7,str),4,18);
	display_graphic_12x16(float_str(v8,str),4,64);
	display_graphic_12x16(float_str(v9,str),4,108);
	display_graphic_12x16("10#  .  11#  .  12#  . ",6,0);
	display_graphic_12x16(float_str(v10,str),6,18);
	display_graphic_12x16(float_str(v11,str),6,64);
	display_graphic_12x16(float_str(v12,str),6,108);
}

void cell_voltage_display1(void)
{
	uint8_t str[3];
	uint16_t v13=0,v14=0,v15=0,v16=0,v17=0,v18=0;
	display_graphic_12x16("13#  .  14#  .  15#  . ",0,0);
	display_graphic_12x16(float_str(v13,str),0,18);
	display_graphic_12x16(float_str(v14,str),0,64);
	display_graphic_12x16(float_str(v15,str),0,108);
	display_graphic_12x16("16#  .  17#  .  18#  . ",2,0);
	display_graphic_12x16(float_str(v16,str),2,18);
	display_graphic_12x16(float_str(v17,str),2,64);
	display_graphic_12x16(float_str(v18,str),2,108);
}

/*用户密码*/
uint8_t user_password[]="123";
void userPassword_input(void)
{
  display_graphic_12x16("用户密码",0,40);
	password_input(user_password,5);			
}

/*用户设置1*/
uint8_t flag[3] = {0};
uint8_t secret_key[8]="00000000";
uint8_t *HM[] = {"关","开"};
uint8_t *force_start[] = {"关","开"};
uint8_t *repeat_switch[] = {"关","开"};
uint8_t *work_status[] = {"主电","电池"};
uint8_t repeat_switch_index = 0;
uint16_t remaining_time = 60, work_status_index = 0, repeat_switch_time = 0;
void user_setting(void)
{
	static uint8_t HM_index=0,force_start_index=0;
	uint8_t temp[4],temp1[9];
	if(page <= 6)
	{
		display_graphic_12x16("》",page,12);
	  display_graphic_12x16("手动应急:",0,24);
		display_graphic_12x16(HM[HM_index],0,96);
	  display_graphic_12x16("强制启动:",2,24);
		display_graphic_12x16(force_start[force_start_index],2,96);
	  display_graphic_12x16("重复转换:",4,24);
		display_graphic_12x16(repeat_switch[repeat_switch_index],4,96);
	  display_graphic_12x16("用户密码:",6,24);
	  display_graphic_12x16(user_password,6,96);
	 }
	else 
	{
		display_graphic_12x16("》",page-8,24);
		display_graphic_12x16("历史记录",0,36);
		display_graphic_12x16("用户密钥",2,36);
		
	}
	
	if(f==1)
	{
		f=0;
		if(page==0)
			HM_index = choose_parameter(HM_index,2,96,HM);
		if(page==2)
			force_start_index = choose_parameter(force_start_index,2,96,force_start);
		if(page==4)
		{
			//repeat_switch_index = choose_parameter(repeat_switch_index,2,96,repeat_switch);
			func_index = 18;
			clrscr();
			page = 0;
			return;
		}
		if(page==6)
		{
			strcpy(temp,user_password);
			if(change_parameter(temp,96,3)==1)
				strcpy(user_password ,temp);
		}
		if(page==8)
			func_index = 14;
		if(page==10)
		{
			clrscr();
			display_graphic_12x16("用户密钥",0,40);
			display_graphic_12x16("请输入密钥:",2,6);
	    
			page = 4;
			strcpy(temp1,secret_key);
			if(change_parameter(temp1,60,8)==1)
				strcpy(secret_key,temp1);
			page = 10;
		}
		clrscr();
	}
	
}
void lcd_backligt(GPIO_PinState state)
{
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0, state);
}
void lcd_screen_save(void)
{
	lcd_backligt(GPIO_PIN_RESET);
	//clrscr();
	display_map(tab1);
}
void repeat_switch_status(void)
{
	uint16_t remaining_time_bak, work_status_index_bak, repeat_switch_time_bak;
	uint8_t string_remaining_time[2];
	uint8_t string_repeat_switch_time[2];
	uint8_t flag=0,flag1=0;
	
	sprintf(string_remaining_time,"%02d",remaining_time);
	sprintf(string_repeat_switch_time,"%02d",repeat_switch_time);
	
	display_graphic_12x16 ("》",0,12);
	display_graphic_12x16("重复转换:",0,24);
	display_graphic_12x16(repeat_switch[repeat_switch_index],0,96);
	display_graphic_12x16("工作状态:",2,24);
	display_graphic_12x16(work_status[work_status_index],2,96);
	display_graphic_12x16("剩余时间:",4,24);
	display_graphic_12x16(string_remaining_time,4,96);
	display_graphic_12x16("转换次数:",6,24);
	display_graphic_12x16(string_repeat_switch_time,6,96);
	
	if(f==1)
	{
		f=0;
		page=0;
		repeat_switch_index = choose_parameter(repeat_switch_index,2,96,repeat_switch);
	}
	
	display_graphic_12x16 ("》",0,12);
	display_graphic_12x16("重复转换:",0,24);
	display_graphic_12x16(repeat_switch[repeat_switch_index],0,96);
	display_graphic_12x16("工作状态:",2,24);
	display_graphic_12x16(work_status[work_status_index],2,96);
	display_graphic_12x16("剩余时间:",4,24);
	display_graphic_12x16(string_remaining_time,4,96);
	display_graphic_12x16("转换次数:",6,24);
	display_graphic_12x16(string_repeat_switch_time,6,96);
	
	while(repeat_switch_index==1)
	{
		display_graphic_12x16(string_remaining_time,4,96);
		display_graphic_12x16(string_repeat_switch_time,6,96);
		if(remaining_time==0&&flag1==0)
		{
			flag1=1;
			remaining_time=21;
		}
		if(remaining_time==0&&flag1==1)
		{
			flag=0;
			remaining_time=61;
			repeat_switch_time++;
		}
		remaining_time--;
		sprintf(string_remaining_time,"%02d",remaining_time);
		sprintf(string_repeat_switch_time,"%02d",repeat_switch_time);
		delay_repeat_switch(&flag);
		if(flag==1)
		{
			clrscr();
			return;
		}
	}
	
}

